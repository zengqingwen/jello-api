bamboo_crawler_apps_common = {
    "label": "fetch user's bamboo project[as a member of the project]", # 该次获取的简介信息
    "params": {"my_apps": "true", "page": "1", "page_size": "10"},
    # "data": "",
    # "headers": {"X-TOKEN": bamboo_token},
    "headers": {"X-TOKEN": "%s"},
    "is_pagination": True, # 貌似可写可不写
    "request_executor": "BambooCrawlerApps", # 利用反射来执行，或者利用eval()来执行   # fetch用于GET，push用于POST？待定
}

entry_types = {
    "bamboo_crawler_apps_dev": dict({
        "url": "http://dev.jcdp.jd.com/rest_api/v1/apps/",
    }, **bamboo_crawler_apps_common),
    "bamboo_crawler_apps": dict({
        "url": "http://bamboo.jd.com/rest_api/v1/apps/",
    }, **bamboo_crawler_apps_common),
}

entry_type_list = [(x, x) for x in entry_types.keys()]
entry_type_list2 = [x for x in entry_types.keys()]

'''
bamboo_token = "BJ.498482ed76d541ae9a92219e423ff2a0"

entries = [
    # {
    #     "label": "fetch user's gitlab repos[as the repo's member]",
    #     "url": "https://git.jd.com/",
    #
    # },
    {
        "label": "fetch user's bamboo project[as a member of the project]", # 该次获取的简介信息
        "url": "http://dev.jcdp.jd.com/rest_api/v1/apps/",
        "params": {"my_apps": "true", "page": "1", "page_size": "10"},
        # "data": "",
        "headers": {"X-TOKEN": bamboo_token},
        "is_pagination": True, # 貌似可写可不写
        "request_executor": "BambooCrawlerApps", # 利用反射来执行，或者利用eval()来执行   # fetch用于GET，push用于POST？待定
    },
    {
        "label": "fetch user's bamboo project[as a member of the project]", # 该次获取的简介信息
        "url": "http://bamboo.jd.com/rest_api/v1/apps/",
        "params": {"my_apps": "true", "page": "1", "page_size": "10"},
        # "data": "",
        "headers": {"X-TOKEN": bamboo_token},
        "is_pagination": True, # 貌似可写可不写
        "request_executor": "BambooCrawlerApps", # 利用反射来执行，或者利用eval()来执行   # fetch用于GET，push用于POST？待定
    }
]
'''
