from rest_framework import serializers
from data_table.event.models import Entry, Credential
from django.db import transaction
from rest_framework import serializers

class EntrySerializer(serializers.ModelSerializer):
    token_name = serializers.SerializerMethodField()

    class Meta:
        model = Entry
        fields = '__all__'

    def get_token_name(self, obj):
        return obj.token.name


class CredentialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Credential
        # exclude = ('token', 'creator')
        fields = '__all__'

    @transaction.atomic
    def create(self, validated_data):
        # # Get the requesting user
        # user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            creator = request.user
        else:
            raise serializers.ValidationError('Must be authenticated to create thread')

        validated_data["creator"] = creator

        return super(CredentialSerializer, self).create(validated_data)