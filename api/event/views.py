from django.contrib.auth import get_user_model
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

from data_table.event.models import *
from .serializers import *
from .utils.event_variables import entry_type_list2

import datetime

User = get_user_model()

class EventFetchAPIView(views.APIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    throttle_scope = 'fetch_events'

    def get(self, request, *args, **kwargs):
        req_params = request.query_params.dict()
        print(req_params)
        ret = {
            "shortcut_outputs": ["http://example.com", "http://example2.com"],
            "events_outputs": [
                {
                    "id": 13,
                    "title": 'Multi-day Event',
                    # "start": 1575015035387,
                    # "start": '2019-11-29T16:10:00',
                    # "start": '2019-11-29',
                    "start": datetime.date.today(),
                    # "end": "2019-12-02",
                    "end": datetime.date.today(),
                    "type": 'gitlab-userevents',
                    "todoevent": True,
                },
                {
                    "id": 14,
                    "title": 'Today',
                    "end": '2019-12-02T16:10:00',
                    "start": '2019-12-02',
                },
            ],
            "err_and_excepts": ["Error1: url error", "Error2: network error"]
        }
        return Response(ret, status=HTTP_200_OK)


class EntryViewSet(viewsets.ModelViewSet):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer
    # authentication_classes = []
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        ret = {"code": 0, "msg": "fetch entries info", "data": None}
        req_params = request.query_params.dict()
        print(req_params)

        ret["data"] = {}
        # all entry list
        ret["data"]["entries"] = entry_type_list2

        # my entry list
        myentries = EntrySerializer(instance=request.user.entry_user.all(), many=True).data
        myentries = [myentry['name'] for myentry in myentries]
        ret["data"]["myentries"] = myentries

        notmyentries = [{"name": ent, "token_name": "", "ismyentry": False} for ent in entry_type_list2 if ent not in myentries]

        myentries2 = EntrySerializer(instance=request.user.entry_user.all(), many=True).data
        myentries2 = [dict({"ismyentry": True}, **ent) for ent in myentries2]

        ret["data"]["myentries2"] = notmyentries + myentries2
        mycredentials = CredentialSerializer(instance=request.user.cre_user.all(), many=True).data
        ret["data"]["mycredentials"] = [{"key": credential["id"], "text": credential["name"], "value": credential["name"]} for credential in mycredentials]
        return Response(ret, status=HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        req_data = request.data
        print(req_data)

        entry_query = Entry.objects.filter(name=req_data['name'])
        if len(entry_query) > 1:
            raise serializers.ValidationError('This entry is more than one queryset your budget!')

        # token: ["Incorrect type. Expected pk value, received Credential."]
        try:
            token = Credential.objects.get(name=req_data['token_name']).id
        except:
            raise serializers.ValidationError('please choose a credential token!')
        # token = Credential.objects.get(name=req_data['token_name'])
        request.data.update({'token': token})

        # creator: ["Incorrect type. Expected pk value, received User."]
        creator = request.user
        request.data['creator'] = creator.id
        # request.data['creator'] = creator
        if len(entry_query) == 1:
            # update
            serializer = self.serializer_class(instance=entry_query[0], data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
        else:
            # create
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
        return Response(serializer.data)

class CredentialViewSet(viewsets.ModelViewSet):
    serializer_class = CredentialSerializer
    queryset = Credential.objects.all()
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        ret = CredentialSerializer(instance=request.user.cre_user.all(), many=True).data
        mycredentials = [{"key": credential["id"], "text": credential["name"], "value": credential["name"]} for credential in ret]
        return Response(mycredentials, status=HTTP_200_OK)
