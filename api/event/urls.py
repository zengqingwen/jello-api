from django.urls import path, include

from rest_framework_jwt.views import obtain_jwt_token

from .views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'entry', EntryViewSet)
router.register(r'credential', CredentialViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
    path('event/', EventFetchAPIView.as_view(), name='fetch-event'),
    # path('entry/', EntryFetchAPIView.as_view(), name='fetch-entry'),
]
