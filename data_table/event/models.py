from django.db import models
from django.contrib.auth import get_user_model


from api.event.utils.event_variables import entry_type_list

# Create your models here.

User = get_user_model()

class Credential(models.Model):
    name = models.CharField(max_length=30, verbose_name="credential名称")  # 给entry起的名称
    token = models.CharField(max_length=256, verbose_name="token")  # 对接entry的token

    creator = models.ForeignKey(User, related_name='cre_user', on_delete=models.SET_NULL, null=True, blank=True)

class Entry(models.Model):
    name = models.CharField(max_length=30, choices=entry_type_list, verbose_name="entry名称")  # 给entry起的名称

    token = models.ForeignKey(Credential, related_name='entry_token', on_delete=models.SET_NULL, null=True, blank=True)  # 对接entry的token
    creator = models.ForeignKey(User, related_name='entry_user', on_delete=models.SET_NULL, null=True, blank=True)  # 删除级联
