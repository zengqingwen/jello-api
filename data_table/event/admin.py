from django.contrib import admin
from .models import *

# Register your models here.

class CredentialAdmin(admin.ModelAdmin):
    list_display = ('name', 'token', 'creator',)

class EntryAdmin(admin.ModelAdmin):
    list_display = ('name', 'token', 'creator',)

admin.site.register(Credential, CredentialAdmin)
admin.site.register(Entry, EntryAdmin)
