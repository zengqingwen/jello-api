from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    remark = models.CharField(max_length=128, default='', blank=True)

    class Meta:
        ordering = ['-id']
        verbose_name_plural = u'用户'

    def __unicode__(self):
        return self.username
