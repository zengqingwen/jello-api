from rest_framework import views
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

# Create your views here.

class CurrentUserAPIView(views.APIView):

    def get(self, request, *args, **kwargs):
        print(request.user)
        return Response(request.user.username, status=HTTP_200_OK)